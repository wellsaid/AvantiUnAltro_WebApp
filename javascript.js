//VARIABILI GLOBALI PER GESTIONE GIOCO
punteggio = 0; //punteggio totalizzato dall'utente
n_domanda = 1; //domanda a cui sta rispondendo l'utent
sec_rimanenti = 80; //secondi rimanenti al game over
domande = new Array(); //array multidimensionale 20x4. Colonne: 0 = id 1 = domanda 2 = risposta_esatta 3 = risposta_errata
l_domande = 0; //lunghezza array domande
//------------------------------------

LUNG_TAB_DOMANDE_DB = 32; //lunghezza della tabella domande nel database
gioco_in_pausa = false; //boolean che identifica se il gioco � fermo oppure no
clock = null; //contatore di secondi del gioco

//appende un nuovo figlio di tipo elem all'oggetto "padre", dopo avergli assegnato tutte le propriet�
//come indicato nei due array paralleli "nomi_attributi" e "valori_attributi"
//immete come testo dell'oggetto figlio la stringa "text"
//infine, restituisce l'oggetto figlio creato
function creaFiglio(padre,elem, nomi_attributi, valori_attributi, l_array_proprieta, text){
	figlio = document.createElement(elem);
	figlio.appendChild(document.createTextNode(text));
	for(i=0; i < l_array_proprieta; i++)
		figlio.setAttribute(nomi_attributi[i],valori_attributi[i]);
	padre.appendChild(figlio);
	return figlio;
}

//ripulisce il contenuto dell'oggetto div_contenuto eliminandone tutti i figli
function RipulisciContenuto(){
	contenuto = document.getElementById("div_contenuto");
	contenuto.firstChild.nodeValue = "";
	p = contenuto.firstChild.nextSibling;
	while(p != null){
		q = p;
		p = p.nextSibling;
		contenuto.removeChild(q);
        }
}

//Scrive le istruzioni all'interno del div contenuto
function MostraIstruzioni(){
				 RipulisciContenuto();
				 contenuto = document.getElementById("div_contenuto");
				 contenuto.firstChild.nodeValue = "Su questo sito puoi giocare la fase finale del gioco televisivo avanti un'altro!"+
"Per vincere devi rispondere dare la risposta sbagliata a 20 domande. Se dai quella giusta devi ricominciare da capo."+
"Hai 80 secondi (4 per domanda). Per ogni risposta sbagliata ottieni +100 punti, per ogni risposta giusta -50 punti."+
"Se riesci a rispondere a tutte le domande entro il tempo limite ottieni +500 punti di bonus e +30 punti per ogni secondo avanzato."+
"A fine partita puoi salvare il tuo punteggio sul database attraverso un nickname, per confrontarti con gli altri giocatori";
				 creaFiglio(contenuto,"input",new Array("type","id","class","value","onclick"),new Array("button","but_indietro","small_button","<","MostraMenuPrincipale()"),5,"");
				 document.getElementById("div_contenuto").style.visibility = "visible";
				 document.getElementById("div_contenuto").style.overflowY = "hidden";
				 document.getElementById("div_contenuto").style.overflowX = "hidden";
}

//Carica l'interfaccia di gioco
function CaricaGioco(){
				 RipulisciContenuto();
				 gioco_in_pausa = false;
				 document.getElementById("div_bottoni").style.visibility = "hidden";
				 contenuto = document.getElementById("div_contenuto");
				creaFiglio(contenuto,"div",new Array("id"),new Array("div_n_dom"),1,"Domanda n�1");
				creaFiglio(contenuto,"div",new Array("id"),new Array("div_tempo"),1,"Tempo rimanente: 80 sec");
				creaFiglio(contenuto,"div",new Array("id","class"),new Array("div_dom","div_con_bordo"),2,"");
				creaFiglio(contenuto,"div",new Array("id"),new Array("riga_dom"),1,"");
				creaFiglio(contenuto,"input",new Array("type","id","class","onclick"),new Array("button","but_risp_1","but_game","ControllaRisposta(1)"),4,"");
				creaFiglio(contenuto,"input",new Array("type","id","class","onclick"),new Array("button","but_risp_2","but_game","ControllaRisposta(2)"),4,"");
				creaFiglio(contenuto,"input",new Array("type","id","class","onclick","value"),new Array("button","but_pausa","small_button","Pausa()","P"),5,"");
				creaFiglio(contenuto,"input",new Array("type","id","class","onclick","value"),new Array("button","but_uscita","small_button","UscitaForm(\"CaricaGioco()\")","X"),5,"");
				creaFiglio(contenuto,"div",new Array("id"),new Array("div_punteggio"),1,"Punteggio: 0");
				contenuto.style.visibility = 'visible';
				contenuto.style.overflowY = "hidden";
				contenuto.style.overflowX = "hidden";
				modifica_punteggio("=",0); //Stampa il punteggio contenuto nella variabile
				CreaCaselle();
				MostraDomanda();
				TempoGioco();
}

//Crea le caselle sotto il riquadro della domanda, impostandole su una riga
function CreaCaselle(){ //<TODO> Usare css per fare caselline si potrebbe incazzare
				 riga = document.getElementById("riga_dom");
				 for(k=1; k<21; k++)
					creaFiglio(riga,"div",new Array("id","class","style"),new Array("div_dom_"+k,"div_dom_i","left:"+(k*4)+"%"),3,k);
}

/*Mette in evidenza che si sta rispondendo alla domanda con indice_dom
sollevando la relativa casellina e colorandola in giallo*/
function Evidenzia(){
				 i = 1;
				 for(; i < n_domanda; i++){
				 				 div_i = document.getElementById("div_dom_" + i).style;
								 div_i.backgroundColor = "green";
								 div_i.top = "60%";
				 }
				 for(; i <= l_domande; i++){
				 				 div_i = document.getElementById("div_dom_" + i).style;
								 div_i.top = "60%";
								 if(i == l_domande) div_i.backgroundColor = "red";
								 else div_i.backgroundColor = "green";
				 }
				 for(; i < 21; i++){
				 				 div_i = document.getElementById("div_dom_" + i).style;
								 div_i.top = "60%";
								 div_i.backgroundColor = "#B1B2B2";
				 }
				 div_i = document.getElementById("div_dom_" + n_domanda);
				 div_i.style.backgroundColor = "yellow";
				 div_i.style.top = "45%";
}

//Handler per il timer di gioco, e la fine allo scadere del tempo
function TempoGioco(){
				 document.getElementById("div_tempo").firstChild.nodeValue = "Tempo rimanente: "+ sec_rimanenti +" sec";
				 if(sec_rimanenti == 0) 
				 			GameOver();
				 else{
				 			sec_rimanenti--;
				 			clock = setTimeout("TempoGioco()",1000);
				 }
}

//funzione che copia all'interno del div contenuto il risultato della query per 
//mostrare la tabella punteggi
function useHttpResponse_tabella(){ //<TODO> Cambiare risposta della pagina php e funzione usando creaFiglio invece di innerHTML
				 if(xmlHttp.readyState == 4){
															 RipulisciContenuto();
															 document.getElementById("div_contenuto").innerHTML = xmlHttp.responseText + "<input type='button' id='but_indietro' class='round_button' value='<' onclick='MostraMenuPrincipale()'>";
															 document.getElementById("div_contenuto").style.visibility = "visible";
															 document.getElementById("div_contenuto").style.overflowY = "visible";
															 document.getElementById("div_contenuto").style.overflowX = "hidden";
															 document.getElementById("div_bottoni").style.visibility = "hidden";
				 }
}

//In caso di errori in inserimento o aggiornamento punteggio utente
//li mostra nel div_errori oppure mostra un messaggio di successo
function useHttpResponse_inserimento(nickname){
				 if(xmlHttp.readyState == 4){
				 												 if(xmlHttp.responseText != ""){
											 RipulisciContenuto();					 												 document.getElementById("div_errori").firstChild.nodeValue = xmlHttp.responseText;
				}
																 else
																 		 										 esegui_query("SELECT * FROM punteggi ORDER BY punteggio DESC&nickname="+nickname,4);
				 }
}

//Legge il risultato tramite xmHttp per la richiesta di una nuova domanda
//e lo inserisci nel vettore domande
function useHttpResponse_domanda(){
				 				 		 				 		 domande[n_domanda] = new Array();
                              	 var indice = 0;
                              	 for(i=0; i < xmlHttp.responseText.length; i++){
                              	 					if(xmlHttp.responseText.charAt(i) != '|'){
                              																			 if(domande[n_domanda][indice] == undefined) domande[n_domanda][indice] = "";
                              																			 domande[n_domanda][indice] += xmlHttp.responseText.charAt(i);
                              						}
  																				else indice++;
           											 };
}
																									
//Funzione che esegue query sul database e chiama l'opportuno metodo in base a op
function esegui_query(query,op,nickname){

				 xmlHttp = null;
				 try { xmlHttp=new XMLHttpRequest(); }
         catch (e)
         { try { xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); }
         catch (e)
         { try { xmlHttp=new ActiveXObject("Microsoft.XMLHTTP"); }
         catch (e)
         { window.alert("Your browser does not support AJAX!");
           return;
         }}}
				 
				 xmlHttp.open("POST","esegui_query.php",false);
				 xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
         xmlHttp.send("query="+query+"&op="+op);
				 
				 if(op == 1) useHttpResponse_domanda(); //Richiesta di una nuova domanda
				 if(op == 3) useHttpResponse_inserimento(nickname); //Inserimento punteggio
				 if(op == 4) useHttpResponse_tabella(); //Stampa tabella punteggi
}

//funzione che genera una domanda, facendo attenzione che non � gi� presente nell'array domande
function genera_domanda(){
				 x = 0;
				 do
				 		x = Math.floor((Math.random()*LUNG_TAB_DOMANDE_DB)+1); //Restituisce un numero random tra 1 e LUNG_TAB_DOMANDE_DB
				 while(scelta(x));
				 esegui_query("SELECT * FROM domande WHERE id="+x,1);
}

//Controlla che la domanda relativa all'indice non sia contenuto nell'array domande
//Restituisce true se la trova false altrimenti
function scelta(indice){
				 if(n_domanda == 1) return false;
				 for(i=1; i<l_domande; i++)
				 					if(domande[i][0] == indice)
																	 return true;
				 return false;
}

//Usa le variabili globali per gestire il gioco
function MostraDomanda(){
				 if(n_domanda > l_domande){ l_domande++; genera_domanda(); }
				 document.getElementById("div_dom").firstChild.nodeValue = domande[n_domanda][1];
				 esatta = Math.floor((Math.random()*2)+1);
				 document.getElementById("but_risp_1").value = domande[n_domanda][(esatta==1)?2:3];
 				 document.getElementById("but_risp_2").value = domande[n_domanda][(esatta==2)?2:3];
				 document.getElementById("div_n_dom").firstChild.nodeValue = "Domanda n�"+n_domanda;
				 Evidenzia();
}

//viene chiamato da uno dei due bottoni risposta
//controlla che la risposta data sia giusta o sbagliata
//se giusta passa alla prossima, se sbagliata torna all'inizio
function ControllaRisposta(indice){
				 if(document.getElementById('but_risp_'+indice).value == domande[n_domanda][3]){ //risposta errata
											 if(n_domanda == l_domande) modifica_punteggio('+',100);
											 n_domanda++;
				 }
				 else{ //risposta esatta
											 n_domanda = 1;
											 modifica_punteggio('-',50);
				 }
				 if(n_domanda == 21) //si � risposto a tutte le domande
				 							GameOver();
				 else
				 		 					MostraDomanda();
}

//funziona che modifica il punteggio in base a op (se + o -) e a val
//e lo mostra all'utente
function modifica_punteggio(op,val){
				 if(op == '+')
				 			 punteggio += val;
				 else
				 		 	 punteggio -= val;
				 document.getElementById('div_punteggio').firstChild.nodeValue = "Punteggio: " + punteggio;
}

//Pone fine al gioco e mostra il menu di salvataggio punteggio
function GameOver(){
				 testo = "";
				 if(sec_rimanenti == 0)
				 									testo = "Game Over!";
				 else{
				 									modifica_punteggio('+',500+(sec_rimanenti*30));
													testo = "Vittoria! Punteggio: " + punteggio;
				 }
				 div_menu = creaFiglio(document.getElementById('div_contenuto'),"div",new Array("id","class"),new Array("div_menu","div_con_bordo"),2,testo);
				 creaFiglio(div_menu,"input",new Array("type","id","class","value","onclick"),new Array("button","but_save","but_game_menu","Salva Punteggio","SalvaPunteggioForm()"),5,"");
				 creaFiglio(div_menu,"input",new Array("type","id","class","value","onclick"),new Array("button","but_exit","but_game_menu","Esci","UscitaForm(\"CaricaGioco(); GameOver();\")"),5,"");
				 bottoni_off();
				 clearInterval(clock);
}

//Funzione che disabilit� i bottoni di gioco
function bottoni_off(){
				 		 document.getElementById("but_risp_1").onclick = "";
     				 	         document.getElementById("but_risp_2").onclick = "";
    				                 document.getElementById("but_pausa").onclick = "";
						 document.getElementById("but_uscita").onclick = "";
}

//Crea il form di domanda se si vuole uscire durante una partita
function UscitaForm(funzione_rimane){
				 clearInterval(clock);
				 div_menu = creaFiglio(document.getElementById("div_contenuto"),"div",new Array("id","class"),new Array("div_uscita","div_con_bordo"),2,"Sei sicuro di voler uscire? Il tuo punteggio non verr� salvato! :(");
				 creaFiglio(div_menu,"input",new Array("type","class","value","onclick"),new Array("button","but_game_menu","Si","MostraMenuPrincipale()"),4,"");
				 creaFiglio(div_menu,"input",new Array("type","class","value","onclick"),new Array("button","but_game_menu","No",funzione_rimane),4,"");
				 if(document.getElementById("but_risp_1") != null) bottoni_off();
}

//Se il gioco non � in pausa lo ferma, se lo � lo sblocca
function Pausa(){
				 if(gioco_in_pausa)
				 			CaricaGioco();
				 else{
				 			gioco_in_pausa = true;
				 			div_menu = "<div id='div_menu' class='div_con_bordo'> PAUSA!"+
				      				   "<input type='button' id='but_save' class='but_game_menu' value='Riprendi' onclick='Pausa()'>"+
				 								 "<input type='button' id='but_exit' class='but_game_menu' value='Esci' onclick='UscitaForm(\"CaricaGioco(); Pausa();\")'></div>";
							document.getElementById("div_contenuto").innerHTML += div_menu;
							bottoni_off();
							clearInterval(clock);
				 }
				 
}

//Crea la form per immettere i dati
function SalvaPunteggioForm(){
				 RipulisciContenuto();
				 document.getElementById("div_contenuto").innerHTML = "Punteggio: " + punteggio +
				 																											" Nickname: <input type='text' id='nickname_text'>"+
																															" <input type='button' value='Salva' onclick='salva_punteggio()'>"+
																															" <input type='button' value='Esci' onclick='UscitaForm(\"SalvaPunteggioForm()\")'>"+
																															" <div id='div_errori'> </div>";
}

//controlla che i campi nickname e password non sia nulli ed esegue le query sul database per aggiungere il punteggio
//in caso di controllo positivo esegue la query
function salva_punteggio(){
				 if(document.getElementById("nickname_text").value == "")
				 												document.getElementById("div_errori").firstChild.nodeValue = "il campo username non pu� essere vuoto";
				 esegui_query("INSERT INTO punteggi VALUES('"+document.getElementById("nickname_text").value+"',"+punteggio+")",3,document.getElementById("nickname_text").value);
}

//Ritorna al menu principale
function MostraMenuPrincipale(){
				 document.getElementById("but_gioca").style.visibility = "visible";
				 document.getElementById("but_istruzioni").style.visibility = "visible";
				 document.getElementById("but_record").style.visibility = "visible";
				 document.getElementById("div_contenuto").style.visibility = "hidden";
				 punteggio = 0;
				 n_domanda = 1; 
				 sec_rimanenti = 80; 
    		 domande = new Array();
				 l_domande = 0;
}
