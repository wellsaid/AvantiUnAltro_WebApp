SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
CREATE SCHEMA IF NOT EXISTS `db_avanti_unaltro` DEFAULT CHARACTER SET latin1 ;
USE `mydb` ;
USE `db_avanti_unaltro` ;

-- -----------------------------------------------------
-- Table `db_avanti_unaltro`.`domande`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_avanti_unaltro`.`domande` (
  `id` INT(11) NOT NULL ,
  `domanda` VARCHAR(50) NOT NULL ,
  `risposta_esatta` VARCHAR(10) NOT NULL ,
  `risposta_errata` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('15', 'Ferro o Neon, quale e\' un metallo?', 'Ferro', 'Neon');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('16', 'Viola o Violino, quale e\' un fiore?', 'Viola', 'Violino');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`) VALUES ('17');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('18', 'Pesce rosso o criceto, chi gira la ruota?', 'Criceto', 'Pesce rosso');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('19', 'Poltrona o sgabello, quale � pi� scomodo?', 'sgabello', 'Poltrona');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('20', 'Barile o Ancora, quale si getta in mare?', 'Ancora', 'Barile');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('21', 'Computer o macchina, cosa si usa per programmare?', 'Computer', 'Macchina');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('22', 'Mano o Piede, dove si trova il pollice opponibile?', 'Mano', 'Piede');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('23', 'Reppublica o Monarchia, L\'inghilterra � una?', 'Monarchi', 'Repubblica');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('24', 'Liscio o Ruvido, la carta vetrata e\'?', 'Ruvido', 'Liscio');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('25', 'Munch o Le Courbusier, Chi ha dipinto l\'urlo?', 'Munch', 'Le Courbusier');INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('26', 'Norvegia o Svezia, Stoccolma � la capitale della?', 'Svezia', 'Norvegia');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('27', 'Ananas o Fragola, quale frutto ha il tipico colore rosso?', 'Fragola', 'Ananas');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('28', 'Ottagono o quadrato, chi ha meno lati?', 'Quadrato', 'Ottagono');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('29', 'Kawasaki o Honda, chi produce la Ninja?', 'Kawasaki', 'Honda');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('30', 'Stoner o Rossi, chi � il Dottore?', 'Rossi', 'Stoner');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('31', '9 o 10, quanti mondiali ha vinto Valentino Rossi?', '9', '10');
INSERT INTO `db_avanti_unaltro`.`domande` (`id`, `domanda`, `risposta_esatta`, `risposta_errata`) VALUES ('32', 'Cassata o Castagnaccio, quale dolce � tipico della Sicilia?', 'Cassata', 'Castagnaccio');

-- -----------------------------------------------------
-- Table `db_avanti_unaltro`.`punteggi`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_avanti_unaltro`.`punteggi` (
  `nickname` VARCHAR(30) NULL DEFAULT NULL ,
  `punteggio` INT(11) NULL DEFAULT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
